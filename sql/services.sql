-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 05, 2017 at 10:46 AM
-- Server version: 5.6.31-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `codeigniter`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE  IF NOT EXISTS `services` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`description` VARCHAR(255) NOT NULL,
	`title` VARCHAR(50) NOT NULL,
	`price` INT(11) NULL DEFAULT NULL,
	`image` VARCHAR(50) NULL DEFAULT NULL,
	`video` VARCHAR(50) NULL DEFAULT NULL,
	`category` VARCHAR(50) NULL DEFAULT NULL,
	`country` VARCHAR(50) NULL DEFAULT NULL,
	`longitude` VARCHAR(50) NULL DEFAULT NULL,
	`latitude` VARCHAR(50) NULL DEFAULT NULL,
	`phone` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;


--
-- Dumping data for table `users`
--

INSERT INTO `service_bd`.`services` (`title`, `description`, `price`, `category`, `country`) VALUES
('Service first', 'Service de livraison\r\n', '10', 'Livraison', 'Mada');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
