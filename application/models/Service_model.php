<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Service_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_alls($where=array(),$start=false,$limit=false)
    {
        $this->db->where($where);
        if($limit){
            $this->db->limit($start,$limit);
        }
        $this->db->order_by('id','desc');
        $query = $this->db->get('services');
        return $query->result();
    }

    public function get_one($id) {
        $this->db->where("id", $id);
        $this->db->limit(1);
        return $this->db->get("services")->result();
    }

    public function insert_entry($data = array()) {
        return $this->db->insert('services', $data);
    }

    public function update_entry($data)
    {
        $this->db->update('services', $data, array('id' => $data["id"]));
        if($this->db->affected_rows()){
            return true;
        } else {
            return false;
        }
    }
}

