<?php
require APPPATH . 'libraries/REST_Controller.php';

class Services extends REST_Controller {

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model(array("service_model"));
        $this->load->library(array("form_validation"));
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function index_get() {
        $whereClause = array();
        if ($this->input->get("id")) {
            $whereClause["id"] = $this->input->get("id");
        }
        $services = $this->service_model->get_alls($whereClause);
        if ($services) {
            $json=array(
                'success' => true,
                'data' => $services,
                'status_code' => REST_Controller::HTTP_OK,
                'message'=>'Service found'
            );
            // Set the response and exit
            $this->response($json, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            // Set the response and exit
            $json = array(
                'success' => false,
                'status_code' => REST_Controller::HTTP_NOT_FOUND,
                'message'=>'not found',
                "data" => $services
            );
            $this->response($json, REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function index_post() {
        $json = array('success' => false);
        $post = $this->post();

        $service_id = $this->service_model->insert_entry($post);
        if($service_id){
            $json = array(
                'success' => true,
                'status_code' => REST_Controller::HTTP_CREATED,
                'message' => 'Service added'
            );
            $this->set_response($json, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        }
    }

    public function index_put()
    {
        $json = array('success'=>false);
        $post = $this->put();

        $service_id = $this->service_model->update_entry($post);
        if($service_id){
            $json = array(
                'success' => true,
                'status_code' => REST_Controller::HTTP_CREATED,
                'message'=>'Service updated'
            );
            $this->set_response($json, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
        }
    }
}
